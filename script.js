$(document).ready(function () {
    $('#userSlider').carousel();
});
window.addEventListener('scroll', () => {
    if (window.innerHeight + window.scrollY >= document.body.offsetHeight) {
        // Call the function to load more data (from lazy-load.js)
        loadMoreData();
    }
});
// Get the slider element and pricing plans
// Get the slider element and pricing plans
const userSlider = document.getElementById('user-slider');
const plan1 = document.querySelector('.col-md-4:nth-child(1) .card');
const plan2 = document.querySelector('.col-md-4:nth-child(2) .card');
const plan3 = document.querySelector('.col-md-4:nth-child(3) .card');

// Add an event listener to the slider
userSlider.addEventListener('input', () => {
    const sliderValue = parseInt(userSlider.value);

    // Check which range the slider value falls into and highlight the corresponding plan
    if (sliderValue >= 0 && sliderValue <= 10) {
        highlightPlan(plan1);
    } else if (sliderValue > 10 && sliderValue <= 20) {
        highlightPlan(plan2);
    } else if (sliderValue > 20 && sliderValue <= 30) {
        highlightPlan(plan3);
    } else {
        // Clear highlights if the value is outside the specified range
        clearHighlights();
    }
});

// Function to highlight a plan
function highlightPlan(planElement) {
    clearHighlights(); // Clear highlights from all plans
    planElement.classList.add('highlighted'); // Add a CSS class to highlight the plan
}

// Function to clear highlights from all plans
function clearHighlights() {
    plan1.classList.remove('highlighted');
    plan2.classList.remove('highlighted');
    plan3.classList.remove('highlighted');
}