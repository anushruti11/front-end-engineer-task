// lazy-load.js

const lazyLoadContainer = document.querySelector('.lazy-load-container');
const resultsContainer = lazyLoadContainer.querySelector('.results');

let page = 1;

const fetchData = async () => {
    try {
        const response = await fetch(`https://jsonplaceholder.typicode.com/posts?_page=${page}&_limit=10`);
        const data = await response.json();
        return data;
    } catch (error) {
        console.error('Error fetching data:', error);
        return [];
    }
};

const displayData = (data) => {
    data.forEach((item) => {
        const resultItem = document.createElement('div');
        resultItem.classList.add('result-item');
        resultItem.innerHTML = `
            <h3>${item.title}</h3>
            <p>${item.body}</p>
        `;
        resultsContainer.appendChild(resultItem);
    });
};

const loadMoreData = async () => {
    const newData = await fetchData();
    displayData(newData);
    page++;
};

// Load initial data
loadMoreData();

// Implement lazy loading when the user scrolls to the bottom of the container
lazyLoadContainer.addEventListener('scroll', () => {
    if (lazyLoadContainer.scrollTop + lazyLoadContainer.clientHeight >= resultsContainer.clientHeight) {
        loadMoreData();
    }
});
